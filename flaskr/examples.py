
import os
from flask import Flask, make_response, session, redirect, url_for, request, render_template, render_template_string, jsonify
from surukuku import endpoint, Surukuku

     
if os.path.split(os.getcwd())[-1] == "flaskr":
    app = Flask("Test")
else:
    app = Flask("Test", template_folder="flaskr/templates", static_folder="flaskr/static")

@endpoint(app, "first/<x>/<y>")
class A():
    """[summary]

    Args:
        x (int): [description]
        y (int): [description]
    """

    def post(x, y):
        """
        [description]

        Args:
            name (string): [description]
            ooo (string): [description]
        """
        if request.method == "POST":
            test = request.get_json()
            return make_response(f"{test['name']} = {int(x) ** int(y)} -> {test['ooo']}", 200)
        else:
            headers = {"Content-Type": "application/json"}
            return make_response(
                str(int(x) * int(y)),
                200,
                headers
            )

    def get(x, y):
        """
        [description]
        """
        if request.method == "GET":
            headers = {"Content-Type": "application/json"}
            return make_response(
                str(int(x) * int(y)),
                200,
                headers
            )

@endpoint(app, "first/second")
class B():
    """[summary]
    """

    def get():
        """[summary]

        Args:
            name (string): [description]
        """
        if request.method == "GET":
            headers = {"Content-Type": "application/text"}
            return make_response(
                f"Hello {request.args['name']}!!!",
                200,
                headers
            )

class SecondEndpoint(Surukuku):
    """[summary]

    Args:
        x (int): [description]
        y (int): [description]
    """

    path = "Second Endpoint/<x>/<y>"
    app = app

    def get(x, y):
        """[summary]
        """
        if request.method == "GET":
            headers = {"Content-Type": "application/json"}
            return make_response(
                str(int(x) ** int(y)),
                200,
                headers
            )


if __name__ == '__main__':
    app.jinja_env.auto_reload = True
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.run()