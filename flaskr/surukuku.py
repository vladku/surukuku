import os
import inspect
from doc_parser import DocParser
from flask import make_response, render_template

class Meta(type):
    methods_to_doc = {}
    def __new__(cls, name, bases, dct):
        x = super().__new__(cls, name, bases, dct)
        if name == "Surukuku":
            return x
        methods = inspect.getmembers(x, predicate=inspect.isfunction or inspect.ismethod)
        endpoint = x.path if hasattr(x, "path") else name
        doc = Meta._get_doc(endpoint, x)
        for m_name, method in methods:
            Meta.update_method(x, endpoint, doc, m_name, method)
            
        setattr(x, "doc", (x.app.route(f'/api'))(Meta.doc))
        setattr(x, "doc_json", (x.app.route(f'/api_json'))(Meta.doc_json))
        setattr(x, "error404", (x.app.errorhandler(404))(Meta.not_found))
        return x

    def _get_doc(endpoint, x):
        dd = None
        doc = Meta.methods_to_doc
        endpoints = os.path.normpath(endpoint).split(os.sep)
        ref = ""
        for e in endpoints:
            if e not in doc:
                ref = f"{ref}/{e}"
                doc.update({e: {"methods":{}, "sub_endpoints":{}, "ref": ref}})
            if "sub_endpoints" in doc[e]:
                dd = doc[e]
                doc = doc[e]["sub_endpoints"]
        dd.update(DocParser(x.__doc__).to_json())
        dd.update({"methods":{}, "sub_endpoints":{}})
        return dd

    def update_method(x, endpoint, doc, name, method):
        #params = [f"<{x}>" for x in inspect.signature(method).parameters]
        #route = f'/{endpoint}/{"/".join(params)}'
        route = f'/{endpoint}'
        endpoints = os.path.normpath(endpoint).split(os.sep)
        # if "methods" not in doc:
        #     doc.update({})
        doc["methods"].update({name: {}})
        doc["methods"][name].update(DocParser(method.__doc__).to_json()) 
        doc["methods"][name].update({"url": route })
        doc["methods"][name].update({"endpoint": endpoints[-1] })
        setattr(x, name, (x.app.route(route, endpoint=f"{endpoint}{name}", methods=[name.upper()]))(method))

    def doc():
        return render_template("/api.html", doc=Meta.methods_to_doc)

    def doc_json():
        return make_response(Meta.methods_to_doc)
        
    def not_found(e):
        """Page not found."""
        return make_response(render_template("404.html"), 404)
        

class Surukuku(metaclass=Meta):
    pass

def endpoint(app_in, path_in):
    def sub(Cls):
        class NewCls(Cls, Surukuku):
            app = app_in
            path = path_in
            __doc__ = Cls.__doc__
        return NewCls
    return sub
