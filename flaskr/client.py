import json
import requests

class Client():
    def __init__(self, url):
        self.url = url
        self.r = json.loads(requests.get(f"{url}/api_json").text)
        
    def exec(self, endpoint, m_name, **kwargs):
        method = self.r[endpoint]["methods"][m_name]
        url = self._get_url(endpoint, method['url'], kwargs)
        
        if m_name == "get":
            return requests.get(url).text
    
    def _get_url(self, endpoint, url_part, kwargs):
        params = [x["name"] for x in self.r[endpoint]["params"]]
        for p in params:
            url_part = url_part.replace(f"<{p}>", str(kwargs[p]))
        return f"{self.url}/{url_part}"
    
    def __getattribute__(self, name):
        if name.startswith("__") or name in self.__dict__:
            return object.__getattribute__(self, name)
        if name in self.r:
            endpoint = self.r[name]
            return Endpoint(endpoint, self.url)
        return object.__getattribute__(self, name)

class Endpoint():
    def __init__(self, json, url):
        self.json = json
        self.url = url

    def get(self, **kwargs):
        url_part = self.json["methods"]["get"]['url']
        for p in [x["name"] for x in self.json["params"]]:
            url_part = url_part.replace(f"<{p}>", str(kwargs[p]))
        url = f"{self.url}/{url_part}"
        return requests.get(url).text

client = Client("http://127.0.0.1:5000")
print(client.exec("first", "get", x=5, y=9))
print(client.first.get(x=5, y=9))