$(document).ready(function() {
    $(".apiTest").click(function() {
        var button = $(this);
        var paramSelector = "." + button.attr('param');
        urlStr = button.attr('url');
        $(paramSelector).each(function(index, element){
            urlStr = urlStr.replace("<" + $(this).attr('name') + ">", $(this).val());
        });
        dataSelector = "." + button.attr('param') + "_body";
        requestType = $(this).attr('type');
        console.log(requestType);
        if(requestType === "get" || requestType === "delete"){
            dataJson = "";
            $(dataSelector).each(function(index, element){
                if(dataJson !== ""){
                    dataJson += "&";
                }
                dataJson += $(this).attr('name') + "=" + $(this).val();
            });
        } else {
            dataJson = "{";
            $(dataSelector).each(function(index, element){
                if(dataJson !== "{"){
                    dataJson += ",";
                }
                dataJson += "\"" + $(this).attr('name') + "\":\"" + $(this).val() + "\"";
            });
            //console.log(dataSelector);
            //console.log(urlStr);
            dataJson += "}";
        }
        $.ajax({
            url: urlStr,
            type: requestType,
            headers: { 'Access-Control-Allow-Origin': '*',},
            data: dataJson,
            // data: JSON.stringify({
            //     name: 'Test'
            // }),
            contentType: "application/json",
            success: function(data, textStatus, jqXHR){
                response = $(button).closest("div").find(".response");
                response.addClass("active");
                response.removeClass("error");
                response.html(`HTTP/1.1 ${jqXHR.status} ${textStatus}
                    <br/><span class="tab"></span>${data}`);
            },
            error: function(jqXHR, textStatus, error){
                response = $(button).closest("div").find(".response");
                response.addClass("active error");
                response.html(`HTTP/1.1 ${jqXHR.status} ${error}`);
            }
        });
    });
});