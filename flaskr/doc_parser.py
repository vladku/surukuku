import re
import json
from json import JSONEncoder

class DocParser:
    def __init__(self, doc_str):
        if doc_str is None:
            return
        doc = re.compile(r"(?P<doc>(\n{0,1}.)*)(\n\n\s*Args:|$)", re.M)
        param = re.compile(r"^\s*Args:(\s*(?P<name>\S+)\s\((?P<type>[^)]*)\):\s(?P<description>.+))*", re.M)
        params = re.compile(r"^\s*(?P<name>\S+)\s\((?P<type>[^)]*)\):\s(?P<description>.+)", re.M)
        self.description = doc.match(doc_str).group("doc").strip()
        if param.search(doc_str):
            self.params = [{k:v.strip() for (k, v) in m.groupdict().items()} 
                for m in params.finditer(param.search(doc_str)[0])]
        else:
            self.params = None

    def __str__(self):
        return json.dumps(self, indent=4, cls=CustomEncoder)

    def to_json(self):
        return json.loads(str(self))

class CustomEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__